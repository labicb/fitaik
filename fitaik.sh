#!/bin/bash
#
# Master script to run the "FitAik" package,
# to calculate Einstein coefficients by least-square
# fitting with experimental data.
#
# See "readme.pdf" for a user's guide.
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#


##########################################
####             Parameters           ####
##########################################


# They are set in the local "param.sh" subshell, for each species
source param.sh


###################################################
####          Functions declarations           ####
###################################################


# Scans all s.f.s to call Rcg
scan_scal_fact_cowan () {
  local -i  j k
  local     DELAY
  local -ai sf_all

  DELAY=0.4
  I_RCG=0
  echo " Checks th. files with various scaling factors..."

  for j in `seq 1 1 $NB_SF_TOT`; do
    sf_all[$j]=$SF_MIN_COW
  done
  run_rcg_process ${sf_all[*]} &
  I_RCG=$(($I_RCG + 1))
  for k in `seq 1 1 $NB_SF_TOT`; do
    for j in `seq 1 1 $NB_SF_TOT`; do
      sf_all[$j]=$SF_MIN_COW
    done
    sf_all[$k]=$(($SF_MIN_COW+$SF_STEP_COW))
    NAME_TH_SF_FILE=$(./print_name_spec_th_file ${sf_all[*]} < $NAME_TMP_FILE)
    if [ ! -s $NAME_TH_SF_FILE ]; then
      sleep $DELAY
    fi
    run_rcg_process ${sf_all[*]} &

    I_RCG=$(($I_RCG + 1))
    sf_all[$k]=$((${sf_all[$k]}+$SF_STEP_COW))
    NAME_TH_SF_FILE=$(./print_name_spec_th_file ${sf_all[*]} < $NAME_TMP_FILE)
    if [ ! -s $NAME_TH_SF_FILE ]; then
      sleep $DELAY
    fi
    run_rcg_process ${sf_all[*]} &

    I_RCG=$(($I_RCG + 1))
  done
  wait
  echo " RCG calculations done";  echo
}

# For each set of scaling factors, run Rcg and process results
run_rcg_process() {
  x=$@

  # First prints name of th spec file
  NAME_TH_SF_FILE=$(./print_name_spec_th_file ${x} < $NAME_TMP_FILE)

  # Tests the existance of file with theoretical spectrum
  if [ ! -s $NAME_TH_SF_FILE ]; then
    # --------------------------
    #  If not, it is calculated
    # --------------------------

    # Generates ing11 file and moves it to "cowan" directory
    ./write_file_ing11 ${x} < $NAME_TMP_FILE 1>/dev/null 2>>$REP_SH/fitaik.log
    print_error_message "write_file_ing11"
    mkdir -p $REP_COWAN/$I_RCG
    mv $ATOM$EXT_ING $REP_COWAN/$I_RCG

    # Now runs Rcg
    cd $REP_COWAN/$I_RCG
    echo "  Run RCG for s.f.(s) = ${x} ..."
    if [ $RCG_K = 1 ]; then
      $REP_SH/rcgk.sh $ATOM 2>>$REP_SH/fitaik.log 1>/dev/null
      awk -f $REP_SH/write_outgk_spec.awk $ATOM.OUT 1>$ATOM'.spec' 2>>$REP_SH/fitaik.log
    else
      cowan.sh $RCG_SIZE_OPT -m rcg $ATOM 2>>$REP_SH/fitaik.log 1>/dev/null
    fi
    print_error_message "cowan"
    cd $REP_SH
    awk -f filter_spec.awk -v n_com=$NB_LIN_COM_TH $REP_COWAN'/'$I_RCG'/'$ATOM'.spec' > $NAME_TH_SF_FILE

    # ---------------------------
    #  End of spectr. file calc.
    # ---------------------------
  fi
}

# Prints error message if problem
print_error_message () {
  if [ $? != 0 ]; then
    echo ""
    echo " ERROR in \"$1\" execution. Calculation stopped !"
    read -p " Print log file (y/n)?" ans
    if [ $ans = "y" ]; then
      cat fitaik.log
    fi
    echo ""
    return 1
  fi
}


###########################################################
####          End of functions declaractions           ####
###########################################################


# Display license message 
echo
echo "    FitAik  Copyright (C) 2021,2022  Maxence Lepers"
echo "        This program comes with ABSOLUTELY NO WARRANTY; for details type 'less COPYING'."
echo "        This is free software, and you are welcome to redistribute it"
echo "        under certain conditions; type 'less COPYING' for details."
echo

# Compilation
echo
echo " Compilation..."
make -i clean   1>/dev/null 2>&1
#make -i cleanup 1>/dev/null
make all 1>/dev/null
echo " Done"
echo

# Some parameters
REP_SH=$(pwd)
REP_COWAN=$REP_SH'/cowan/'
NAME_TMP_FILE="tmp.in"
NAME_TMP_FILE_SF="tmp_sf.in"

# Variables depending on the Cowan version to run
if [ $RCG_K = 1 ]; then
  EXT_ING=".ING11"
  EXT_ING_MOD=".ING11_MODEL"
else
  EXT_ING=".ing11"
  EXT_ING_MOD=".ing11_model"
fi


######################################################
####          Prints file with namelist           ####
######################################################


echo "&list_param" > $NAME_TMP_FILE
echo '  atom = "'$ATOM'"' >> $NAME_TMP_FILE
echo '  name_inp_file_ener_1   = "'$ATOM'_'$PATTERN_ENER'_1.txt"' >> $NAME_TMP_FILE
echo '  name_inp_file_ener_2   = "'$ATOM'_'$PATTERN_ENER'_2.txt"' >> $NAME_TMP_FILE
echo '  parity_1               = "'$PARITY_1'"'                   >> $NAME_TMP_FILE
echo '  parity_2               = "'$PARITY_2'"'                   >> $NAME_TMP_FILE
echo '  form_data_ener         = "'$FORM_ENER'"'                  >> $NAME_TMP_FILE
echo '  name_inp_file_spec_exp = "'$ATOM'_'$PATTERN_EXP'"'        >> $NAME_TMP_FILE
echo '  form_data_spec_exp     = "'$FORM_EXP'"'                   >> $NAME_TMP_FILE
echo '  order_data_spec_exp    = "'$ORDER_EXP'"'                  >> $NAME_TMP_FILE
echo '  rand_Aik_exp           = '$RAND_EXP                       >> $NAME_TMP_FILE
echo '  rand_Aik_exp_prctg     = '$RAND_PRCTG                     >> $NAME_TMP_FILE
echo '  unit_Aik_exp           = '$UNIT_AIK_ENER                  >> $NAME_TMP_FILE
echo '  name_inp_file_spec_th  = "'$ATOM'_'$PATTERN_TH'"'         >> $NAME_TMP_FILE
echo '  form_data_spec_th      = "'$FORM_TH'"'                    >> $NAME_TMP_FILE
echo '  order_data_spec_th     = "'$ORDER_TH'"'                   >> $NAME_TMP_FILE
echo '  name_file_index_line   = "'$ATOM'_'$PATTERN_INDEX_LINE'"' >> $NAME_TMP_FILE
echo '  pattern_file_spec_sf   = "'$PATTERN_TH_SF'"'              >> $NAME_TMP_FILE
echo '  pattern_out_ing11      = "'$EXT_ING'"'                    >> $NAME_TMP_FILE
echo '  pattern_model_ing11    = "'$EXT_ING_MOD'"'                >> $NAME_TMP_FILE
echo '  sf_denom               = '$SF_DENOM                       >> $NAME_TMP_FILE
echo '  nb_sf_free             = '$NB_SF_FREE                     >> $NAME_TMP_FILE
echo '  nb_sf_tot              = '$NB_SF_TOT                      >> $NAME_TMP_FILE
echo '  nb_sf_cow              = '$NB_SF_COW                      >> $NAME_TMP_FILE
echo '  sf_min_cow             = '$SF_MIN_COW                     >> $NAME_TMP_FILE
echo '  sf_step_cow            = '$SF_STEP_COW                    >> $NAME_TMP_FILE
printf '  tab_sf_min             = '                              >> $NAME_TMP_FILE
for i in ${SF_MIN[*]}; do
  printf "$i,"                                                    >> $NAME_TMP_FILE
done
printf "\n"                                                       >> $NAME_TMP_FILE
printf '  tab_sf_max             = '                              >> $NAME_TMP_FILE
for i in ${SF_MAX[*]}; do
  printf "$i,"                                                    >> $NAME_TMP_FILE
done
printf "\n"                                                       >> $NAME_TMP_FILE
printf '  tab_sf_step            = '                              >> $NAME_TMP_FILE
for i in ${SF_STEP[*]}; do
  printf "$i,"                                                    >> $NAME_TMP_FILE
done
printf "\n"                                                       >> $NAME_TMP_FILE
echo '  name_file_fit_aik      = "'$FILE_FIT_AIK'"'               >> $NAME_TMP_FILE
printf '  tab_sf_line            = '                              >> $NAME_TMP_FILE
for i in ${SF_LINE[*]}; do
  printf "$i,"                                                    >> $NAME_TMP_FILE
done
printf "\n"                                                       >> $NAME_TMP_FILE
printf '  tab_sf_cstr            = '                              >> $NAME_TMP_FILE
for i in ${SF_CSTR[*]}; do
  printf "$i,"                                                    >> $NAME_TMP_FILE
done
printf "\n"                                                       >> $NAME_TMP_FILE
echo '  name_out_stddev_file   = "'$FILE_STD_DEV'"'               >> $NAME_TMP_FILE
echo '  name_out_sf_opt_file   = "'$FILE_SF_OPT'"'                >> $NAME_TMP_FILE
echo '/'                                                          >> $NAME_TMP_FILE


######################################################################
####          Scans SFs (in recursive "calc_scal_fact")           ####
######################################################################


# Checks Cowan directory
if [ ! -d $REP_COWAN ]; then
  mkdir $REP_COWAN
fi

# Fistly checks existence of reference file with th. lines
# Then indexes theoretical lines
echo " Checks reference file with th. lines..."
FILE_REF_TH=$ATOM"_"$PATTERN_TH
if [ -s $FILE_REF_TH ]; then
  echo " File $FILE_REF_TH exists"
else
  # If not runs cowan with ing11_model file
  cp $ATOM$EXT_ING_MOD $REP_COWAN/$ATOM$EXT_ING
  cd $REP_COWAN
  echo "  Run Rcg..."
  if [ $RCG_K = 1 ]; then
    $REP_SH/rcgk.sh $ATOM 2>>$REP_SH/fitaik.log 1>/dev/null
    awk -f $REP_SH/write_outgk_spec.awk $ATOM.OUT 1>$ATOM'.spec' 2>>$REP_SH/fitaik.log
  else
    cowan.sh $RCG_SIZE_OPT -m rcg $ATOM 2>>$REP_SH/fitaik.log 1>/dev/null
  fi
  print_error_message "cowan"
  echo "  Done"
  cd $REP_SH
  awk -f filter_spec.awk -v n_com=$NB_LIN_COM_TH $REP_COWAN'/'$ATOM'.spec' > $FILE_REF_TH
  echo " Done"
fi
echo

# Then indexes theoretical lines
echo " Indexes theor. lines with exp. counterparts..."
FILE_INDEX_TH="$ATOM"'_'"$PATTERN_INDEX_LINE"
if [ -s $FILE_INDEX_TH ]; then
  echo " File $FILE_INDEX_TH exists"
else
  ./index_th_lines < $NAME_TMP_FILE 1>/dev/null 2>>$REP_SH/fitaik.log
  print_error_message "index_th_lines"
  echo " Indexation done"
fi &
echo

# Runs Rcg calculations
scan_scal_fact_cowan

# Calculates coefficients in front of s.f.s
echo " Extracts coefs. between Aik's and s.f.(s)..."
if [ -s $FILE_FIT_AIK ]; then
  echo " File $FILE_FIT_AIK exists"
else
  ./fit_aik < $NAME_TMP_FILE 1>/dev/null 2>>$REP_SH/fitaik.log
  print_error_message "fit_aik"
  echo " Done"
fi
echo


# For all sets of free s.f.s, calculates std deviation
echo " Calculates standard deviations..."
echo

# Header of file with opt sf
printf "#" > $FILE_SF_OPT
printf "%-$((7*$NB_SF_FREE-1))s" " Opt.scal.fact." >> $FILE_SF_OPT
printf "%-$((7*$NB_SF_FREE))s" " Uncert." >> $FILE_SF_OPT
printf "Lin.StdDev." >> $FILE_SF_OPT
printf "%-$((7*$NB_SF_FREE))s" " Opt.scal.fact." >> $FILE_SF_OPT
printf "%-$((7*$NB_SF_FREE))s" " Uncert." >> $FILE_SF_OPT
printf "Log.StdDev.\n" >> $FILE_SF_OPT
# Checks if random factor is applied on exp. Aik
declare -ai sf_opt_all
if [ $RAND_EXP = "T" ]; then
  echo "  Random exptal Aik calculated with $NB_RAND runs..."
  for i in $(seq 1 1 $NB_RAND); do
    sleep 0.2
    ./calc_std_dev < $NAME_TMP_FILE 1>/dev/null 2>>$REP_SH/fitaik.log &
    print_error_message "calc_std_dev"
  done
  wait
  # Calculates avreage values of optimal s.f.s
  echo
  echo " Average optimal s.f.s for linear std.dev."
  >$NAME_TMP_FILE_SF
  for i in $(seq 1 1 $NB_SF_FREE); do
    awk -f calc_aver_sf.awk -v i_sf=$i -v i_col=$i -v sf_den=$SF_DENOM \
      -v file_tmp=$NAME_TMP_FILE_SF $FILE_SF_OPT $FILE_SF_OPT
  done
  printf "  Aver lin. std.dev. = "
  awk -v i=$((2*$NB_SF_FREE+1)) -v n=$NB_RAND \
    '{s+=$i}END{printf ("%.3E\n",s/n)}' $FILE_SF_OPT
  # For the optimal s.f.s, writes an ing11 file in the $REP_COWAN directory
  echo ; echo "  Ecriture du fichier ${ATOM}_opt.ing11"
  read -a SF_OPT < $NAME_TMP_FILE_SF
  for i in $(seq 0 1 $(($NB_SF_TOT-1))); do
    sf_opt_all[$i]=SF_OPT[$((${SF_CSTR[$i]}-1))]
  done
  ./write_file_ing11 ${sf_opt_all[*]} < $NAME_TMP_FILE 1>/dev/null 2>>$REP_SH/fitaik.log
  print_error_message "write_file_ing11"
  mv $ATOM'.ing11' $REP_COWAN/$ATOM'_opt.ing11'
  echo
  echo " Average optimal s.f.s for log. std.dev."
  >$NAME_TMP_FILE_SF
  for i in $(seq $((2*$NB_SF_FREE+2)) 1 $((3*$NB_SF_FREE+1))); do
    ii=$(($i-2*$NB_SF_FREE-1))
    awk -f calc_aver_sf.awk -v i_sf=$ii -v i_col=$i -v sf_den=$SF_DENOM \
      -v file_tmp=$NAME_TMP_FILE_SF $FILE_SF_OPT $FILE_SF_OPT
  done
  printf "  Aver log. std.dev. = "
  awk -v i=$((4*$NB_SF_FREE+2)) -v n=$NB_RAND \
    '{s+=$i}END{printf ("%.3E\n",s/n)}' $FILE_SF_OPT
  echo
else
  echo "  No random exptal Aik calculated"
  ./calc_std_dev < $NAME_TMP_FILE 2>>$REP_SH/fitaik.log
  print_error_message "calc_std_dev"
  # For the optimal s.f.s, writes an ing11 file in the $REP_COWAN directory
  >$NAME_TMP_FILE_SF
  for i in $(seq 1 1 $NB_SF_FREE); do
    awk -f calc_aver_sf.awk -v i_sf=$i -v i_col=$i -v sf_den=$SF_DENOM \
      -v file_tmp=$NAME_TMP_FILE_SF $FILE_SF_OPT $FILE_SF_OPT > /dev/null
  done
  read -a SF_OPT < $NAME_TMP_FILE_SF
  for i in $(seq 0 1 $(($NB_SF_TOT-1))); do
    sf_opt_all[$i]=SF_OPT[$((${SF_CSTR[$i]}-1))]
  done
  echo "  Writes the ${ATOM}_opt$EXT_ING file"; echo
  ./write_file_ing11 ${sf_opt_all[*]} < $NAME_TMP_FILE 1>/dev/null 2>>$REP_SH/fitaik.log
  print_error_message "write_file_ing11"
  mv $ATOM$EXT_ING $REP_COWAN/$ATOM'_opt'$EXT_ING
fi
echo " Done"
echo

# Unset array variables
unset SF_MIN SF_MAX SF_STEP SF_OPT TRANS_1EL SF_LINE SF_CSTR

