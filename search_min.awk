# search_min.awk
#
# Searches the minimal value(s) of a given file column
# (col, given in argument of this script), and prints
# the corresponding line(s).
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

BEGIN{
  nb_lin_ign = 2
  min = 1.e12
#  col = 3
  nb_min = 1
}

( (FNR>nb_lin_ign) && ($0!="") ) {
  # Here there is new minimum, so the line array is again filled
  # starting from its 1st element
  if ( $col < min ) {
    nb_min = 1
    line[nb_min] = $0
    min = $col
  }
  # Here, the minimum is "degenerate", so keeps on filling array "line"
  else if ( $col == min ) {
    nb_min = nb_min + 1
    line[nb_min] = $0
  }
}

END {
  printf ( " Lines(s) where minimum(a) of col. %d is(are) found :\n", col ) 
  for ( i=1; i<=nb_min ; i++ ) {
   print line[i]
  }
}
