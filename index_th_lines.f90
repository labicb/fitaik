! index_th_lines.f90
! 
! For each experimental transition (file name in input)
! indicates the line of the corresponding transition in theoretical
! file calculated by Cowan (*.spec).
! 
! Copyright 2021,2022 Maxence Lepers
! 
! LICENSE:
! 
!    This file is part of FitAik.
! 
!    FitAik is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    any later version.
! 
!    FitAik is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
! 
!    You should have received a copy of the GNU General Public License
!    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
! 

program index_th_lines
  use param
  implicit none

  ! Variables characterizing the experimental levels, including their th counterpart
  integer :: nb_lev_exp_1, nb_lev_exp_2
  integer, dimension(:), allocatable :: tab_ener_exp_1, tab_ener_th_1, tab_j_1, &
                                        tab_ener_exp_2, tab_ener_th_2, tab_j_2

  ! Other varibales
  character(len=64) :: name_out_file
  integer :: i1, i2, i3, i4, j_exp_1, j_exp_2, e_exp_1, e_exp_2, e_th_1, e_th_2, &
             e_tmp_1, j_tmp_1, e_tmp_2, j_tmp_2
  real :: r1, r2, r3, r4
  character(len=96) :: s1, s2, s3, s4
  logical :: test_open_file


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !          Reads and prints parameters          !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! Read from namelist /last_param/ in std input (hence the 5)
  call read_namelist_param ( 5 )
  ! Then prints them in stdout
  write ( *, fmt='("")' )
  write ( *, fmt='(" Files with energy levels of 1st/2nd parity :")' )
  write ( *, fmt='(" ",2(" ",A))' ) trim(name_inp_file_ener_1), &
                                     trim(name_inp_file_ener_2)
  write ( *, fmt='(" 1st/2nd parity :",2(" ",A))' ) trim(parity_1), trim(parity_2)
  write ( *, fmt='(" F90 format for energy data : ",A)' ) trim(form_data_ener)

  write ( *, fmt='(" File with exp lines : ",A)' ) trim(name_inp_file_spec_exp)
  write ( *, fmt='(" F90 format for exp lines : ",A)' ) trim(form_data_spec_exp)
  write ( *, fmt='(" Order for exp data : ",A)' ) trim(order_data_spec_exp)

  write ( *, fmt='(" File with th lines : ",A)' ) trim(name_inp_file_spec_th)
  write ( *, fmt='(" F90 format for th lines : ",A)' ) trim(form_data_spec_th)
  write ( *, fmt='(" Order for th data : ",A)' ) trim(order_data_spec_th)
  write ( *, fmt='("")' )

!  write ( unit=6, fmt='("")' )
!  read ( *, * ) name_inp_file_ener_1, name_inp_file_ener_2
!  write ( *, fmt='(" Files with energy levels of 1st/2nd parity :")' )
!  write ( *, fmt='(" ",2(" ",A))' ) trim(name_inp_file_ener_1), &
!                                     trim(name_inp_file_ener_2)
!  read ( *, * ) parity_1, parity_2
!  write ( *, fmt='(" 1st/2nd parity :",2(" ",A))' ) trim(parity_1), trim(parity_2)
!  read ( *, fmt='(A96)' ) form_data_ener
!  write ( *, fmt='(" F90 format for energy data : ",A)' ) trim(form_data_ener)
! 
!  read ( *, * ) name_inp_file_spec_exp
!  write ( *, fmt='(" File with exp lines : ",A)' ) trim(name_inp_file_spec_exp)
!  read ( *, fmt='(A96)' ) form_data_spec_exp
!  write ( *, fmt='(" F90 format for exp lines : ",A)' ) trim(form_data_spec_exp)
!  read ( *, fmt='(A96)' ) order_data_spec_exp
!  write ( *, fmt='(" Order for exp data : ",A)' ) trim(order_data_spec_exp)
! 
!  read ( *, * ) name_inp_file_spec_th
!  write ( *, fmt='(" File with th lines : ",A)' ) trim(name_inp_file_spec_th)
!  read ( *, fmt='(A96)' ) form_data_spec_th
!  write ( *, fmt='(" F90 format for th lines : ",A)' ) trim(form_data_spec_th)
!  read ( *, fmt='(A96)' ) order_data_spec_th
!  write ( *, fmt='(" Order for th data : ",A)' ) trim(order_data_spec_th)
!  write ( unit=6, fmt='("")' )


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !      Reads files describing energy levels (from RCE)     !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! First checks their existence
  inquire ( exist=test_open_file, file=trim(name_inp_file_ener_1) )
  if ( .not. test_open_file ) then
    write ( unit=0, fmt=* )
    write ( unit=0, fmt='(" File ",A," not found !")' ) trim(name_inp_file_ener_1)
    write ( unit=0, fmt=* )
    stop 1
  end if
  inquire ( exist=test_open_file, file=trim(name_inp_file_ener_2) )
  if ( .not. test_open_file ) then
    write ( unit=0, fmt=* )
    write ( unit=0, fmt='(" File ",A," not found !")' ) trim(name_inp_file_ener_2)
    write ( unit=0, fmt=* )
    stop 1
  end if

  ! First scans to determine the number of levels
  !  - parity 1
  open ( unit=1, action="read", file=trim(name_inp_file_ener_1) )
  i1 = 0
  do
    read ( unit=1, fmt=form_data_ener, end=1 ) s1, r1, s2, r2, s1, r3
!if ( s2(1:1) .ne. "*" ) print *, r1, r2, r3
    ! Levels with "*" are ignored
    if ( s2(1:1) .ne. "*" ) i1 = i1 + 1
  end do
1 continue
  nb_lev_exp_1 = i1
  write ( *, fmt='(" Nber of exp levels in parity 1 : ",I0)' ) nb_lev_exp_1
  !  - parity 2
  open ( unit=2, action="read", file=trim(name_inp_file_ener_2) )
  i1 = 0
  do
    read ( unit=2, fmt=form_data_ener, end=2 ) s1, r1, s2, r2, s1, r3
!if ( s2(1:1) .ne. "*" ) print *, r1, r2, r3
!    ! Levels with "*" are ignored
    if ( s2(1:1) .ne. "*" ) i1 = i1 + 1
  end do
2 continue
  nb_lev_exp_2 = i1
  write ( *, fmt='(" Nber of exp levels in parity 2 : ",I0)' ) nb_lev_exp_2
  write ( unit=6, fmt='("")' )

  ! Second series of scans to save the levels characteristics
  ! NB: Energies are stored as integers, in units of 0.1*cm-1
  !     J are mulltiplied by 2, and stored as integers.
  ! 
  !  - parity 1
  allocate ( tab_ener_exp_1(nb_lev_exp_1), tab_ener_th_1(nb_lev_exp_1) )
  allocate ( tab_j_1(nb_lev_exp_1) )
  rewind ( unit=1 )
  i1 = 1
  do
    read ( unit=1, fmt=form_data_ener ) s1, r1, s2, r2, s1, r3
    ! Levels with "*" are ignored
    if ( s2(1:1) .ne. "*" ) then
      tab_ener_exp_1(i1) = int ( 1.e4*r1+0.1 )
      tab_ener_th_1 (i1) = int ( 1.e4*r2+0.1 )
      tab_j_1       (i1) = int ( 2.*r3+0.1 )
!print '(3I7)', tab_ener_exp_1(i1), tab_ener_th_1(i1), tab_j_1(i1)
      i1                 = i1 + 1
    end if
    ! Exits if all levels are characterized
    if ( i1 .gt. nb_lev_exp_1 ) exit
  end do
  close ( unit=1 )
  ! 
  !  - parity 2
  allocate ( tab_ener_exp_2(nb_lev_exp_2), tab_ener_th_2(nb_lev_exp_2) )
  allocate ( tab_j_2(nb_lev_exp_2) )
  rewind ( unit=2 )
  i1 = 1
  do
    read ( unit=2, fmt=form_data_ener ) s1, r1, s2, r2, s1, r3
    ! Levels with "*" are ignored
    if ( s2(1:1) .ne. "*" ) then
      tab_ener_exp_2(i1) = int ( 1.e4*r1+0.1 )
      tab_ener_th_2 (i1) = int ( 1.e4*r2+0.1 )
      tab_j_2       (i1) = int (   2.*r3+0.1 )
!print '(3I7)', tab_ener_exp_2(i1), tab_ener_th_2(i1), tab_j_2(i1)
      i1                 = i1 + 1
    end if
    ! Exits if all levels are characterized
    if ( i1 .gt. nb_lev_exp_2 ) exit
  end do
  close ( unit=2 )


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !           Reads files with experimental lines            !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! First checks files existence
  inquire ( exist=test_open_file, file=trim(name_inp_file_spec_exp) )
  if ( .not. test_open_file ) then
    write ( unit=0, fmt=* )
    write ( unit=0, fmt='(" File ",A," not found !")' ) trim(name_inp_file_spec_exp)
    write ( unit=0, fmt=* )
    stop 1
  end if
  inquire ( exist=test_open_file, file=trim(name_inp_file_spec_th) )
  if ( .not. test_open_file ) then
    write ( unit=0, fmt=* )
    write ( unit=0, fmt='(" File ",A," not found !")' ) trim(name_inp_file_spec_th)
    write ( unit=0, fmt=* )
    stop 1
  end if

  name_out_file = trim(name_file_index_line)
  open ( unit=3, action="read", file=trim(name_inp_file_spec_exp) )
  open ( unit=4, action="read", file=trim(name_inp_file_spec_th) )
  open ( unit=7, action="write", file=trim(name_out_file) )
  ! Scans exptal lines
  i3 = 1
  scan_exp: do
    ! Info is stored in temp variables as follows :
    !  - r1/3 : energy of 1st-/2nd-coming level
    !  - r2/4 : J      of 1st-/2nd-coming level
    !  - s2/3 : parity of 1st-/2nd-coming level
    ! s1 and first r1-storage are ignored
    read ( unit=3, fmt=form_data_spec_exp, end=4 ) s4, r1, s1, r1, s1, s2, &
                                                   s1, r2, s1, r3, s1, s3, s1, r4

!    ! Lines starting with "#" are ignored
!    if ( trim(s4(1:1)) .eq. "#" ) then
!      i3 = i3 + 1
!      cycle
!    end if
    ! Checks first parity of first-coming level
    if ( trim(s2) .eq. trim(parity_1) ) then
      ! Checks second-coming parity
      if ( trim(s3) .ne. trim(parity_2) ) then
        call print_err_spec_exp_parity ()
      end if
      ! Second parity is OK
      e_exp_1 = int ( 10.*r1+0.1 )
      j_exp_1 = int ( 2. *r2+0.1 )
      e_exp_2 = int ( 10.*r3+0.1 )
      j_exp_2 = int ( 2. *r4+0.1 )
    else if ( trim(s2) .eq. trim(parity_2) ) then
      ! Checks second parity
      if ( trim(s3) .ne. trim(parity_1) ) then
        call print_err_spec_exp_parity ()
      end if
      ! Second parity is OK
      e_exp_2 = int ( 10.*r1+0.1 )
      j_exp_2 = int ( 2. *r2+0.1 )
      e_exp_1 = int ( 10.*r3+0.1 )
      j_exp_1 = int ( 2. *r4+0.1 )
    else
      call print_err_spec_exp_parity ()
    end if

    ! Now searches the two levels in arrays of exp/th levels
    do i1 = 1, nb_lev_exp_1
      if ( abs(e_exp_1-tab_ener_exp_1(i1)) .le. tol_ener ) then
        if ( j_exp_1 .eq. tab_j_1(i1) ) then
          e_th_1 = tab_ener_th_1(i1)
          exit
        end if
      end if
    end do
!print *, e_th_1
    do i2 = 1, nb_lev_exp_2
      if ( abs(e_exp_2-tab_ener_exp_2(i2)) .le. tol_ener ) then
        if ( j_exp_2 .eq. tab_j_2(i2) ) then
          e_th_2 = tab_ener_th_2(i2)
          exit
        end if
      end if
    end do
    ! If all the array "tab_ener_th_(1,2)" has been scanned, 
    ! the level has not been found
    if ( (i1.eq.(nb_lev_exp_1+1)) .or. (i2.eq.(nb_lev_exp_2+1)) ) then
      write ( unit=6, fmt='()' )
      write ( unit=6, fmt='(" WARNING : no th line found corresponding to :")' )
      write ( unit=6, fmt=&
         '("  E1 = ",F0.1, " cm-1, J1 =",F4.1," ; E2 = ",F0.1, " cm-1, J2 =",F4.1)' ) &
         real(e_exp_1)/10., real(j_exp_1)/2., real(e_exp_2)/10., real(j_exp_2)/2.
      write ( unit=6, fmt='()' )
      i3 = i3 + 1
      cycle scan_exp
    end if

    ! Now that th energies are known, searches them in file
    ! with theoretical lines
    rewind ( unit=4 )
    i4 = 1
    scan_th: do
      ! Info is stored in temp variables as follows :
      !  - r1/3 : energy of 1st-2nd-parity level
      !  - r2/4 : J      of 1st-2nd-parity level
      ! s1 is ignored
      read ( unit=4, fmt=form_data_spec_th, end=3 ) s1, r1, s1, r2, &
                                                    s1, r3, s1, r4
      e_tmp_1 = int ( 1.e4*r1+0.1 )
      j_tmp_1 = int ( 2.  *r2+0.1 )
      e_tmp_2 = int ( 1.e4*r3+0.1 )
      j_tmp_2 = int ( 2.  *r4+0.1 )
      ! 
      ! Checks we have the good pair of levels
      if ( abs(e_tmp_1-e_th_1) .le. tol_ener ) then
        if ( abs(e_tmp_2-e_th_2) .le. tol_ener ) then
          if ( (j_tmp_1.eq.j_exp_1) .and. (j_tmp_2.eq.j_exp_2) ) then
          write ( unit=7, fmt=* ) i3, i4
            exit scan_th
          end if
        end if
      end if
      ! Here the th level is not the good one, go to the next
      i4 = i4 + 1
      cycle scan_th
      ! Here, there is no th line correspoding to the exp one
    3 continue
      write ( unit=6, fmt='()' )
      write ( unit=6, fmt='(" WARNING : no th line found corresponding to :")' )
      write ( unit=6, fmt=&
         '("  E1 = ",F0.1, " cm-1, J1 =",F4.1," ; E2 = ",F0.1, " cm-1, J2 =",F4.1)' ) &
         real(e_exp_1)/10., real(j_exp_1)/2., real(e_exp_2)/10., real(j_exp_2)/2.
      write ( unit=6, fmt='()' )
      exit scan_th
    end do scan_th
    ! Next expt line
    i3 = i3 + 1
  end do scan_exp
4 continue


  ! End of program
  close ( unit=3 )
  close ( unit=4 )
  close ( unit=7 )

 !:::::::::::
  contains
 !:::::::::::
  subroutine print_err_spec_exp_parity
  ! Prints an error message and stops program, if wrong
  ! parities in file with experimental lines.
    write ( unit=0, fmt='()' ) 
    write ( unit=0, fmt='(" ERROR : in file with exp lines,")' ) 
    write ( unit=0, fmt='(" two levels with bad parity :",2(" ",A2))' ) &
               trim(s2), trim(s3) 
    write ( unit=0, fmt='()' ) 
    stop 1
  end subroutine print_err_spec_exp_parity
end program index_th_lines

