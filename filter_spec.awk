# filter_spec.awk
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

BEGIN {
  # Nber of columns in output file
  n_col = 18
  # Energy units in output of Cowan (in cm-1)
  energy_unit = 1000.
  # Maximum energy conserved
  e_max = 60000.
  # Max and min transition energy conserved
  om_min =  4000.
  om_max = 60000.
#  print "Maximum transition energy : ", e 
  e_max  = e_max/energy_unit
  om_min = om_min/energy_unit
  om_max = om_max/energy_unit
}

{
  # Processes lines
  if ( (NF==n_col) && ($2<=e_max) && ($7<=e_max) && ($12<=om_max) && ($12>=om_min) )
  {
    print $0
  }
}

END {
#  print "Nber of lines : " NR
}

