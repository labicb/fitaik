# write_outgk_spec.awk
#
# Extracts electric-dipole spectrum lines
# from Kramida's RCG output file, and put them
# in the *.spec format of Dublin version.
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

BEGIN {
  ptrn_e1 = "ELEC DIP SPECTRUM"
  ptrn_ener = "ENERGIES"
  hdr_spec = "0           E      J   Conf              Ep      Jp  Confp           delta E   lambda(A) S/Pmax**2   gf    log gf gA(sec-1) cf,brnch"
  ptrn_1st_lev = "* * *"
  form_spec = "%6d%10.4f%5.1f  %-11s  %10.4f%5.1f  %-11s  %10.4f%12.4f%8.4f%9.4f%8.3f%10.3E%8.4f \n"

  i_e1 = 0
  i_tr = 0
}

# Search for beginnig of elec. dip. section
($0~ptrn_e1) && ($0~ptrn_ener) {
  i_e1 = 1
  # Prints header lines
  getline; getline
  while ( $0 != "" ) {
    print $0; getline
  }
  print hdr_spec
}

# Search for new 1st-parity level with " * * * " pattern
(i_e1==1) && ($0~ptrn_1st_lev) && ($1=="*") {
  # Extract its characteristics
  E = $4
  J = $5
  Conf = ( $6 " " $7 " " $8 )
#  print E, J, Conf
  getline; getline

  while ( $0 != "" ) {
    i_tr++
    Ep = $2
    Jp = $3
    Confp = ( $4 " " $5 " " $6 )
    deltaE = $7
    lambda = $8
    loggf = $9
    gA = $10
    cf = $11

    # Test i_tr
    if ( (i_tr!=$1) && ($1!~"**") ) {
      print "ERROR in write_outgk_spec.awk :" >> "/dev/stderr"
      print "invalid value i_tr = ", i_tr >> "/dev/stderr"
      exit
    }

    # Prints spectrum line
    i_tr_2 = i_tr - 1000000*(int(i_tr/1000000))
    printf ( form_spec, i_tr_2, E, J, Conf, Ep, Jp, Confp, deltaE, lambda, 0., exp(loggf*log(10.)), loggf, gA, cf )
    getline
  }
}

END {
}

