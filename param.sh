#!/bin/bash
#
# Child shell called inside "fitaik.sh", containing
# the parameters of the calculation.
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#


##########################################
####             Parameters           #### 
##########################################


# Atom name and parities (those of exp data file)
ATOM="Er+"
PARITY_1="ev"
PARITY_2="od"


# Scaling factors (given as integers, divided by SF_DENOM)

NB_SF_FREE=5  # free scalings factors
NB_SF_TOT=10  # all scalings factors, includeing constrained ones
SF_DENOM=1000
SF_REF=$SF_DENOM
declare -ai SF_MIN SF_MAX SF_STEP SF_OPT
declare -a  TRANS_1EL # mono-elec trans "nl-n'l'"
  # arrays of dimension NB_SF_FREE

# Values and nature of free s.f.s
TRANS_1EL[1]='6s-6p(s-p)'
SF_MIN[1]=700
SF_MAX[1]=1000
SF_STEP[1]=50

TRANS_1EL[2]='6s-6p(s2-sp)'
SF_MIN[2]=700
SF_MAX[2]=1000
SF_STEP[2]=50

TRANS_1EL[3]='6s-6p(ds-dp)'
SF_MIN[3]=700
SF_MAX[3]=1000
SF_STEP[3]=50

TRANS_1EL[4]='5d-6p'
SF_MIN[4]=700
SF_MAX[4]=1000
SF_STEP[4]=50

TRANS_1EL[5]='5d-4f'
SF_MIN[5]=700
SF_MAX[5]=1000
SF_STEP[5]=50


# Constraints and position in ing11 file on s.f.s
declare -ai SF_LINE SF_CSTR
SF_LINE=( 56 58 59 62 63 65 66 71 72 73 )
SF_CSTR=(  5  1  5  5  4  2  4  3  4  5 )


# Scaling factors for the Cowan calculation
# 1 RCG calculation for all s.f.s=SF_MIN_COW, and for each s.f. 
# "dimension", NB_SF_COW calculartions with SF_MIN_COW+SF_STEP_COW,
# F_MIN_COW+2*SF_STEP_COW, ...
NB_SF_COW=2
SF_MIN_COW=700
SF_STEP_COW=400


# File names & specification
PATTERN_ENER="Ener_ExpTh"  # file with experimental Einstein coeff.
FORM_ENER='(A3,F10.6,A4,F8.4,A28,F4.1)' # F90 format in exp/th energies
PATTERN_EXP="Spec_exp.txt"  # file with experimental Einstein coeff.
FORM_EXP='(A1,F9.3,A11,F9.3,A1,A2,A1,F3.1,A1,F9.3,A1,A2,A1,F4.1,A1,F7.3,A1,F6.3)'
                       # F90 format in exp. file
ORDER_EXP='E up low A' # order in which the relevant parameters apprear
                       # E=ener difference, A=Einstein coeff.
RAND_EXP="F" # = T(true) if exptal Aik must be multiplied by a random factor
             # to cover their range of uncertainty, =F otherwise
RAND_PRCTG="F" # = T(true) if uncertainties on exptal Aik are given as a
               # percentages, =F(false) otherwise
NB_RAND=200  # nber of such shots
UNIT_AIK_ENER=1.0d6 # unit in s-1 in which Aik are given (f90 double precision)
NB_LIN_COM_EXP=0  # nber of comment lines to ignore in exptal file

# About Cowan calculations
RCG_K=0  #=true (1) if Kramida version, false (0) if Dublin
RCG_SIZE_OPT="-d l"  # RCG option giving matrix size
PATTERN_TH="Spec_th_ref.txt" # file with THEORETICAL Einstein coeff. with s.f.s=1
PATTERN_TH_SF="Spec_th_SFs" # file with THEORETICAL Einstein coeff
FORM_TH='(A8,F8.4,A1,F4.1,A17,F8.4,A1,F4.1,A17,F8.4,A38,E9.3)' # F90 format in Cowan file
ORDER_TH='1 2 E A' # order in which the relevant parameters apprear
                   # 1/2=first/second parity level, E=ener difference,
                   # A=g_up*Einstein coeff.
NB_LIN_COM_TH=6  # nber of comment lines to ignore in Cowan output file
PATTERN_INDEX_LINE="index_th_lines.txt"  # file with indices of exp lines
                                         # in th *.spec file 
PATTERN_STD_DEV="std_dev.dat"
FILE_STD_DEV=$ATOM'_'$PATTERN_STD_DEV
PATTERN_FIT_AIK="fit_aik.txt"
FILE_FIT_AIK=$ATOM'_'$PATTERN_FIT_AIK
PATTERN_SF_OPT="SFs_opt.dat"
FILE_SF_OPT=$ATOM'_'$PATTERN_SF_OPT

