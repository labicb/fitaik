! calc_std_dev.f90
! 
! This program computes the standard error on Einstein coefficients
! between an experimental set of lines, and a theoretical ones computed
! with the Cowan code.
! 
! The values of the scaling factors are SF_CSTR given in arguments,
! Other parameters from namelist given in std input.
! 
! The program generates an ouput file (unit=1, 
! "std-error_SFs.dat"), which contains the scaling factors
! and the standard error on Einstein coefficients.
! At each execution of the program, the calculated std errors
! are appended to the existing files.
! 
! At each execution (for each set of SFs) a file "line-details_$(SFs).dat"
! (unit=2), where $SFs is replaced by the integer values of the scaling 
! factors, is written with various information about each
! provessed line.
! 
! Copyright 2021,2022 Maxence Lepers
! 
! LICENSE:
! 
!    This file is part of FitAik.
! 
!    FitAik is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    any later version.
! 
!    FitAik is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
! 
!    You should have received a copy of the GNU General Public License
!    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
! 

program calc_std_dev
  use param
  implicit none
  integer, parameter :: nb_iter_max = 100
  double precision, parameter :: eps_grad = 1.0d-5, eps_sensit = 5.0d-2
  integer, dimension(:), allocatable :: tab_sf_free, tab_sf_all, &
                                        tab_index_exp_line
  integer, dimension(8) :: tab_date_time
  character(len=128) :: s1, s2, s3, s4, name_file_spec_sf, name_out_file_line_detail
  integer :: i1, i2, i3, nb_exp_line, nb_grid_calc
  real, dimension(:), allocatable :: tab_E, tab_Eup, tab_Jup, tab_Elow, tab_Jlow
  character(len=4), dimension(:), allocatable :: tab_par_up, tab_par_low
  real :: E, Eup, Jup, Elow, Jlow, E1, J1, E2, J2, r1, r2
  character(len=4) :: par_up, par_low
  character(len=3), dimension(4) :: data_order
  double precision, dimension(:,:), allocatable :: tab_slope_sf, jacob_std_dev
  double precision, dimension(:), allocatable :: tab_A_exp, grad_std_dev, &
                                                 d_sf_all, tab_A_exp_rand
  double precision, dimension(:,:), allocatable :: d_sf_opt, d_sf_opt_sensit
  double precision, dimension(3) :: tab_std_dev
  double precision :: A_th, A_exp, std_dev, std_dev_log, av_dev_log, d1, d2, d3
  logical :: file_opened


  ! Read from namelist /last_param/ in std input (hence the 5)
  call read_namelist_param ( 5 )

  ! Then prints them in stdout
!  write ( *, fmt='("")' )
!  write ( *, fmt='(" File with fit of Aik : ",A)' ) trim(name_file_fit_aik)
!  write ( *, fmt='("")' )

  ! Opens files with exptal lines, determines its line length
  open ( unit=1, action="read", file=trim(name_inp_file_spec_exp), &
         form="formatted", access="sequential" )
  read ( unit=1, fmt='(A128)' ) s1
  rec_len_spec_exp = len(trim(s1)) + 1
!write ( 6, * ) rec_len_spec_exp
  close ( unit=1 )

  ! Opens files
  open ( unit=1, action="read", file=trim(name_inp_file_spec_exp), &
         form="formatted", recl=rec_len_spec_exp, access="direct" )
  open ( unit=2, action="read", form="unformatted", file=trim(name_file_fit_aik) )
  open ( unit=3, action="read", file=trim(name_file_index_line) )

  ! NB: file with std dev on grid points is wriiten
  ! on disk only for 1-shot calculation (no random Aik)
  if ( rand_Aik_exp ) then
    open ( unit=7, action="readwrite", form="unformatted", status="scratch" )
  else
    open ( unit=7, action="readwrite", file=trim(name_out_stddev_file) )
    s1 = '("#",A   ," Lin.StDev.  Log.StDev.  Ave.log.dev.")'
    write ( s1(7:9), '(I0)' ) 7*nb_sf_free
    write ( unit=7, fmt=trim(s1) ) " Free Scal. Facts.  "
  end if

  ! Reads indices of exp. transitions, and slopes before s.f.s
  !  - First determines nber of transitions
  ! >>>  Does not take the exptal lines starting with "#", 
  ! >>>  even if they are indexed.
  i1 = 0
  do
    read ( unit=3, fmt=*, end=1 ) i3
    read ( unit=1, fmt=trim(form_data_spec_exp), rec=i3 ) &
            s2, E, s1, E1, s1, s1, s1, J1, s1, E2, s1, s1, s1, J2, &
            s1, r1, s1, r2
    if (s2(1:1).ne."#") i1 = i1 + 1
  end do
1 nb_exp_line = i1
!print *, nb_exp_line
  !  - Then allocates and fills array
  allocate ( tab_slope_sf(nb_sf_tot,nb_exp_line) )
  allocate ( tab_index_exp_line(nb_exp_line) )
  rewind ( unit=3 )
  i1 = 0
  scan_file: do
    read ( unit=3, fmt=*, end=2 ) i3
    read ( unit=2 ) tab_slope_sf(:,(i1+1))
    read ( unit=1, fmt=trim(form_data_spec_exp), rec=i3 ) &
            s2, E, s1, E1, s1, s1, s1, J1, s1, E2, s1, s1, s1, J2, &
            s1, r1, s1, r2
    if (s2(1:1).ne."#") then
      i1 = i1 + 1
      tab_index_exp_line(i1) = i3
      if (i1.eq.nb_exp_line) exit scan_file
    end if
  end do scan_file
2 continue

  ! Retrieves the characteristics of exp. lines, once for all
  allocate ( tab_E(nb_exp_line), tab_Eup(nb_exp_line), tab_Jup(nb_exp_line), &
             tab_par_up(nb_exp_line), tab_Elow(nb_exp_line), tab_Jlow(nb_exp_line), &
             tab_par_low(nb_exp_line), tab_A_exp(nb_exp_line), tab_A_exp_rand(nb_exp_line) )
  ! If required, exp Aik are multiplied by random number,
  ! reflecting their uncertainty
  if ( rand_Aik_exp ) then
    call date_and_time ( values=tab_date_time )
    do i1 = 1, tab_date_time(8)
      call random_number ( r1 )
    end do
    do i1 = 1, nb_exp_line
      call random_number ( tab_A_exp_rand(i1) )
    end do
  else
    tab_A_exp_rand(:) = 5.d-1
  end if
!write ( 9, '(10F7.3)' ) tab_A_exp_rand(1:5)
  rewind ( unit=3 )
  do i1 = 1, nb_exp_line
    ! In exptal spectrum, we keep DeltaE, Eup, Jup, Elow, Jlow, A
!write ( 0, * ) tab_index_exp_line(i1)
    read ( unit=1, fmt=trim(form_data_spec_exp), rec=tab_index_exp_line(i1) ) &
            s2, E, s1, E1, s1, s3, s1, J1, s1, E2, s1, s4, s1, J2, &
            s1, r1, s1, r2
    ! Checks data order
    read ( order_data_spec_exp, * ) data_order(:)
    if ( (trim(data_order(2)).eq."up") .and. (trim(data_order(3)).eq."low") ) then
      Eup     = E1
      Jup     = J1
      par_up  = s3
      Elow    = E2
      Jlow    = J2
      par_low = s4
    else if ( (trim(data_order(2)).eq."low") .and. (trim(data_order(3)).eq."up") ) then
      Eup     = E2
      Jup     = J2
      par_up  = s4
      Elow    = E1
      Jlow    = J1
      par_low = s3
    else
      write ( unit=0, fmt='()' ) 
      write ( unit=0, fmt='(" ERROR : in calc_std_dev,")' ) 
      write ( unit=0, fmt='(" Invalid exptal data order ",A)' ) &
                trim(order_data_spec_exp)
      write ( unit=0, fmt='()' ) 
      stop 4
    end if
    ! Stores line characteristics
    tab_E(i1)       = E
    tab_Eup(i1)     = Eup
    tab_Jup(i1)     = Jup
    tab_par_up(i1)  = trim(par_up)
    tab_Elow(i1)    = Elow
    tab_Jlow(i1)    = Jlow
    tab_par_low(i1) = trim(par_low)
    if ( rand_Aik_exp_prctg ) then
      A_exp = dble(r1) + (2.d0*tab_A_exp_rand(i1)-1.d0) * dble(r2)*dble(r1)
    else
      A_exp = dble(r1) + (2.d0*tab_A_exp_rand(i1)-1.d0) * dble(r2)
    end if
    tab_A_exp(i1) = unit_Aik_exp * A_exp
!print '(3F10.3)', r1, r2, A_exp
  end do

  ! Alloactes arrays necessary for the calculation
  allocate ( tab_sf_free(nb_sf_free), d_sf_opt(2,nb_sf_free) )
  allocate ( tab_sf_all(nb_sf_tot), d_sf_all(nb_sf_tot), &
             d_sf_opt_sensit(2,nb_sf_free) )

  ! Calls recursive subroutine to determine dynamically the free s.f.s
  nb_grid_calc = 0
  call scan_free_scal_fact ( 1 )

  allocate ( grad_std_dev(nb_sf_free), jacob_std_dev(nb_sf_free,nb_sf_free) )

  call find_std_dev_min_grid ( d_sf_opt(:,:) )
  call print_line_detail ( d_sf_opt(1,:), "grid-lin" )
  call print_line_detail ( d_sf_opt(2,:), "grid-log" )

  call find_std_dev_min ( d_sf_opt(1,:), d_sf_opt_sensit(1,:) )
  call print_line_detail ( d_sf_opt(1,:), "opt-lin" )
  std_dev = tab_std_dev(1)
  call find_log_std_dev_min ( d_sf_opt(2,:), d_sf_opt_sensit(2,:) )
  call print_line_detail ( d_sf_opt(2,:), "opt-log" )
  std_dev_log = tab_std_dev(2)

  ! Printing of optimal s.f.s
  s1 = '(2(  F7.3,ES11.3))'
  write ( unit=s1(4:5), fmt='(I2)' ) 2*nb_sf_free
  open ( unit=9, action="write", file=trim(name_out_sf_opt_file), &
         position="append" )
  write ( unit=9, fmt=trim(s1) ) d_sf_opt(1,:), &
                                 d_sf_opt_sensit(1,:), std_dev, &
                                 d_sf_opt(2,:), &
                                 d_sf_opt_sensit(2,:), std_dev_log
  close ( unit=9 )
!print *, d_sf_opt_sensit(2,:)

  ! End of code
  write ( *, * )
  deallocate ( tab_sf_free, tab_sf_all, tab_slope_sf, tab_index_exp_line )
  deallocate ( tab_E, tab_Eup, tab_Jup, tab_par_up, tab_Elow, tab_Jlow, tab_par_low, tab_A_exp )
  deallocate ( grad_std_dev, jacob_std_dev, d_sf_opt, d_sf_all, tab_A_exp_rand )
  deallocate ( d_sf_opt_sensit)

  close ( unit=1 )
  close ( unit=2 )
  close ( unit=3 )
  close ( unit=7 )
 !:::::::::::::::::::::::
 !:::::::::::::::::::::::
 contains
 !:::::::::::::::::::::::
 !:::::::::::::::::::::::
  recursive subroutine scan_free_scal_fact ( i_sf_free )
    integer, intent(in) :: i_sf_free
    integer :: j1

    ! Checks if all free s.f.s have been determined
    if ( i_sf_free .gt. nb_sf_free ) then
      ! ------- Beg. of calculations of std. dev. ------- !
      nb_grid_calc = nb_grid_calc + 1
      call calc_std_dev_core ( (dble(tab_sf_free(:))/dble(sf_denom)), tab_std_dev )
      if ( rand_Aik_exp ) then
        write ( unit=7 ) (dble(tab_sf_free(:))/dble(sf_denom)), &
                         tab_std_dev(:)
      else
        s1 = '(  F7.3,3ES12.4)'
        write ( unit=s1(2:3), fmt='(I2)' ) nb_sf_free
        write ( unit=7, fmt=trim(s1) ) (real(tab_sf_free(:))/real(sf_denom)), &
                                       tab_std_dev(:)
      end if
      ! ------- End of calculations of std. dev. ------ !
    else
      ! Nest free s.f.
      do j1 = tab_sf_min(i_sf_free), tab_sf_max(i_sf_free), tab_sf_step(i_sf_free)
        tab_sf_free(i_sf_free) = j1
        call scan_free_scal_fact ( i_sf_free + 1 )
!print '(50I5)', tab_sf_free(:)
      end do
    end if
  end subroutine scan_free_scal_fact
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine calc_std_dev_core ( l_sf, l_std_dev )
    ! Calculates linear and logarithmic standard deviations and the
    ! average logarithmic deviation on Einstein's coefficients.
    ! Input, array with all s.f.s, ouput the calculated quantites.
    double precision, dimension(:), intent(in) :: l_sf
    double precision, dimension(:), intent(out) :: l_std_dev

    ! First retrieves all scaling factors
    do i1 = 1, nb_sf_tot
      d_sf_all(i1) = l_sf(tab_sf_cstr(i1))
    end do

    ! Scans files with exptal lines and indices
    l_std_dev(1) = 0.d0  ! linear std dev
    l_std_dev(2) = 0.d0  ! log std dev
    l_std_dev(3) = 0.d0  ! log average dev
    do i1 = 1, nb_exp_line
      ! Experimental line characteristics
      E     = tab_E(i1)
      Eup   = tab_Eup(i1)
      Jup   = tab_Jup(i1)
      Elow  = tab_Elow(i1)
      Jlow  = tab_Jlow(i1)
      A_exp = tab_A_exp(i1)
      ! Theor A calculated from fit
      A_th = dot_product ( tab_slope_sf(:,i1), d_sf_all(:) )
      A_th = A_th**2 / dble(2*Jup+1)
      ! Calculates error and ratios
      d1 = A_th / A_exp
      d2 = log10(d1)
      l_std_dev(1) = l_std_dev(1) + (A_th-A_exp)**2
      l_std_dev(2) = l_std_dev(2) + d2**2
      l_std_dev(3) = l_std_dev(3) + d2
    end do

    ! Final calculation of errors and priontings
    l_std_dev(1) = sqrt ( l_std_dev(1) / dble(nb_exp_line-nb_sf_free) )
    l_std_dev(2) = sqrt ( l_std_dev(2) / dble(nb_exp_line-nb_sf_free) )
    l_std_dev(3) = l_std_dev(3) / dble(nb_exp_line-nb_sf_free)
  end subroutine calc_std_dev_core
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine print_line_detail ( l_sf, l_flag )
    ! Prints linear and logarithmic ratios between Aexp and Ath
    ! for all lines in the fit, and for the set of free scaling
    ! factors in input. "l_flag" is a string input variable which 
    ! is put in the name in the output file, to determine if it
    ! comes from an optimisation?, of the linear/logarithmic 
    ! standard deviation.
    double precision, dimension(:), intent(in) :: l_sf
    character(len=*), intent(in) :: l_flag

    ! Deteremines name of file with lines details, opens it
    name_out_file_line_detail = trim(atom) // "_" // trim(pattern_out_line_detail) &
                                           // "_" // trim(l_flag)
    i2 = int(log10(real(sf_denom))+0.1) + 1 ! nber of digits for sfs.
    do i1 = 1, nb_sf_free
      s1 = trim(name_out_file_line_detail) // "_"
      write ( s2, '("(A,I0.",I0,")")' ) i2
      write ( unit=name_out_file_line_detail, fmt=trim(s2) ) &
                trim(s1), int(sf_denom*l_sf(i1)+0.49)
    end do
    name_out_file_line_detail = trim(name_out_file_line_detail) // ".dat"
    open ( unit=8, action="write", file=trim(name_out_file_line_detail) )
    ! Prints header of file with line details
    s1 = "#   Elow plow Jlow     Eup  pup  Jup       E       Aexp        Ath  Ath/Aexp  log10(Ath/Aexp)"
    write ( unit=8, fmt='(A)' ) trim(s1)
    write ( unit=8, fmt='("#",A)' ) repeat('-',(len(trim(s1))-1))

    ! Retrieves all scaling factors
    do i1 = 1, nb_sf_tot
      d_sf_all(i1) = l_sf(tab_sf_cstr(i1))
    end do

    ! Scans files with exptal lines and indices
    do i1 = 1, nb_exp_line
      ! Experimental line characteristics
      E       = tab_E(i1)
      Eup     = tab_Eup(i1)
      Jup     = tab_Jup(i1)
      par_up  = tab_par_up(i1)
      Elow    = tab_Elow(i1)
      Jlow    = tab_Jlow(i1)
      par_low = tab_par_low(i1)
      A_exp   = tab_A_exp(i1)
      ! Theor A calculated from fit
      A_th = dot_product ( tab_slope_sf(:,i1), d_sf_all(:) )
      A_th = A_th**2 / dble(2*Jup+1)
      ! Calculates ratios
      d1 = A_th / A_exp
      d2 = log10(d1)
      ! Prints line details
      write ( unit=8, fmt='(2(F8.0,A5,F5.1),F8.0,2ES11.3,2F7.3)' ) &
                Elow, trim(par_low), Jlow, Eup, trim(par_up), Jup, E, A_exp, A_th, d1, d2
    end do
    close ( unit=8 )
  end subroutine print_line_detail
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine find_std_dev_min_grid ( l_sf_opt )
    ! Calculates the optimal s.f.s among those of the grid sf_min(:), 
    ! sf_max(:), sf_step(:), for which the linear (l_sf_opt(1,*)) 
    ! and logarithmic (l_sf_opt(2,*)) errors are minimal
    double precision, dimension(:,:), intent(out) :: l_sf_opt
    double precision, dimension(:), allocatable :: l_sf
    double precision, dimension(3) :: l_std_dev, l_std_dev_min
    integer :: l_i

    l_std_dev_min(:) = 1.0d12
    l_sf_opt(:,:) = 0.d0 
    allocate ( l_sf(nb_sf_free) )
    rewind ( unit=7 )
    if ( rand_Aik_exp ) then
      do l_i = 1, nb_grid_calc
        read ( unit=7 ) l_sf(1:nb_sf_free), l_std_dev(1:3)
!        if (l_std_dev(1).le.l_std_dev_old(1)) print '(5F7.3,3ES11.3)', l_sf(:), l_std_dev(:)
        if (l_std_dev(1).le.l_std_dev_min(1)) then
          l_sf_opt(1,:) = l_sf(:) 
          l_std_dev_min(1) = l_std_dev(1)
        end if
        if (l_std_dev(2).le.l_std_dev_min(2)) then
          l_sf_opt(2,:) = l_sf(:) 
          l_std_dev_min(2) = l_std_dev(2)
        end if
      end do
    else
      read ( unit=7, fmt=* )
      do
        read ( unit=7, fmt=*, end=3 ) l_sf(:), l_std_dev(:)
!        if (l_std_dev(1).le.l_std_dev_old(1)) print '(5F7.3,3ES11.3)', l_sf(:), l_std_dev(:)
        if (l_std_dev(1).le.l_std_dev_min(1)) then
          l_sf_opt(1,:) = l_sf(:) 
          l_std_dev_min(1) = l_std_dev(1)
        end if
        if (l_std_dev(2).le.l_std_dev_min(2)) then
          l_sf_opt(2,:) = l_sf(:) 
          l_std_dev_min(2) = l_std_dev(2)
        end if
      end do
    3 continue
    end if
    deallocate ( l_sf )

    ! Printings
    write ( *, * )
    write ( *, fmt='("  Optimal grid free s.f.s for linear std. dev. :")' )
    s1 = '(  F7.3)'
    write ( unit=s1(2:3), fmt='(I2)' ) nb_sf_free
    write ( *, fmt=trim(s1) ) l_sf_opt(1,:)
    write ( *, fmt='("  Optimal grid free s.f.s for log.   std. dev. :")' )
    write ( *, fmt=trim(s1) ) l_sf_opt(2,:)
  end subroutine find_std_dev_min_grid
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine calc_grad_std_dev ( l_sf, l_grad )
    ! Calculates the gradient of std dev with respect to free s.f.s
    double precision, dimension(:), intent(in) :: l_sf
    double precision, dimension(:), intent(out) :: l_grad
    integer :: j1, j2, j3

    ! Builds arrays with all s.f.s
    do j1 = 1, nb_sf_tot
      d_sf_all(j1) = l_sf(tab_sf_cstr(j1))
    end do

    ! Scans vector components
    l_grad(:) = 0.d0
    do j2 = 1, nb_exp_line
      Jup   = tab_Jup(j2)
      A_exp = tab_A_exp(j2)
      A_th = dot_product ( tab_slope_sf(:,j2), d_sf_all(:) )
      A_th = A_th**2 / dble(2*Jup+1)
      do j1 = 1, nb_sf_free
        do j3 = 1, nb_sf_tot
          if ( tab_sf_cstr(j3) .eq. j1 ) &
            l_grad(j1) = l_grad(j1) + tab_slope_sf(j3,j2)/sqrt(dble(2*Jup+1)) &
                       * sqrt(A_th) * (A_th-A_exp)
        end do
      end do
    end do
    l_grad(:) = 2.d0 * l_grad(:) / tab_std_dev(1) / dble(nb_exp_line-nb_sf_free)
  end subroutine calc_grad_std_dev
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine calc_jacob_std_dev ( l_sf, l_jac )
    ! Calculates the jacobian matrix of std dev with respect to free s.f.s
    double precision, dimension(:), intent(in) :: l_sf
    double precision, dimension(:,:), intent(out) :: l_jac
    double precision :: A_th_j, A_exp_j, A_th_k, A_exp_k
    real :: Jup_j, Jup_k
    integer :: j1, j2, j3, k1, k2, k3

    ! Builds arrays with all s.f.s
    do j1 = 1, nb_sf_tot
      d_sf_all(j1) = l_sf(tab_sf_cstr(j1))
    end do

    ! Part depending on 1 transition
    l_jac(:,:) = 0.d0
    do j2 = 1, nb_exp_line
      Jup   = tab_Jup(j2)
      A_exp = tab_A_exp(j2)
      A_th  = dot_product ( tab_slope_sf(:,j2), d_sf_all(:) )
      A_th  = A_th**2 / dble(2*Jup+1)
      do j1 = 1, nb_sf_free
        do j3 = 1, nb_sf_tot
          if ( tab_sf_cstr(j3) .eq. j1 ) then
            do k1 = j1, nb_sf_free
              do k3 = 1, nb_sf_tot
                if ( tab_sf_cstr(k3) .eq. k1 ) then
                  l_jac(j1,k1) = l_jac(j1,k1) + (3.d0*A_th-A_exp) &
                   * tab_slope_sf(j3,j2)*tab_slope_sf(k3,j2)/dble(2*Jup+1) &
                   * 2.d0 / tab_std_dev(1) / dble(nb_exp_line-nb_sf_free)
                end if
              end do
            end do
          end if
        end do
      end do
    end do
    ! Part depending on 2 transitions
    do j2 = 1, nb_exp_line
      Jup_j   = tab_Jup(j2)
      A_exp_j = tab_A_exp(j2)
      A_th_j  = dot_product ( tab_slope_sf(:,j2), d_sf_all(:) )
      A_th_j  = A_th_j**2 / dble(2*Jup_j+1)
      do k2 = 1, nb_exp_line
        Jup_k   = tab_Jup(k2)
        A_exp_k = tab_A_exp(k2)
        A_th_k  = dot_product ( tab_slope_sf(:,k2), d_sf_all(:) )
        A_th_k  = A_th_k**2 / dble(2*Jup_k+1)
        ! Scans rows and columns
        do j1 = 1, nb_sf_free
          do j3 = 1, nb_sf_tot
            if ( tab_sf_cstr(j3) .eq. j1 ) then
              do k1 = j1, nb_sf_free
                do k3 = 1, nb_sf_tot
                  if ( tab_sf_cstr(k3) .eq. k1 ) then
                    l_jac(j1,k1) = l_jac(j1,k1) &
                     - 4.d0 / tab_std_dev(1)**3 / dble(nb_exp_line-nb_sf_free)**2 &
                     * tab_slope_sf(j3,j2)/sqrt(dble(2*Jup_j+1)) &
                     * sqrt(A_th_j)*(A_th_j-A_exp_j) &
                     * tab_slope_sf(k3,k2)/sqrt(dble(2*Jup_k+1)) &
                     * sqrt(A_th_k)*(A_th_k-A_exp_k)
                  end if
                end do
              end do
            end if
          end do
        end do
      end do
    end do
    ! Now fills upper triangle
    do j1 = 1, nb_sf_free
      do k1 = 1, (j1-1)
        l_jac(j1,k1) = l_jac(k1,j1)
      end do
    end do
  end subroutine calc_jacob_std_dev
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine find_std_dev_min ( l_sf, l_sf_sensit )
    double precision, dimension(:), intent(inout) :: l_sf, l_sf_sensit
    integer, dimension(:), allocatable :: l_ipiv
    double precision, dimension(:,:), allocatable :: l_a, l_b
    integer :: l_nrhs = 1, l_info, l_iter, j1

    allocate ( l_ipiv(nb_sf_free), l_a(nb_sf_free,nb_sf_free), l_b(nb_sf_free,l_nrhs) )
    l_iter = 0
!print '(I4,10F8.3)', l_iter, l_sf(:)
    call calc_std_dev_core  ( l_sf, tab_std_dev )
    call calc_grad_std_dev  ( l_sf, grad_std_dev )
    call calc_jacob_std_dev ( l_sf, jacob_std_dev )

!print '(I4,100ES10.2)', l_iter, grad_std_dev(:)
!print '(I4,100ES11.3)', l_iter, jacob_std_dev(1,:)
!print '(I4,100ES11.3)', l_iter, jacob_std_dev(2,:)
!print '(I4,100ES11.3)', l_iter, jacob_std_dev(3,:)
!print '(I4,100ES11.3)', l_iter, jacob_std_dev(4,:)
!print '(I4,100ES11.3)', l_iter, jacob_std_dev(5,:)

    ! Checks convergence of error
    do while ( all ( (abs(grad_std_dev(:))) .gt. &
                  (/ (eps_grad, j1=1, nb_sf_free) /) ) )

      l_a(:,:) = jacob_std_dev(:,:)
      l_b(:,1) = -grad_std_dev(:)
      call DGESV ( nb_sf_free, l_nrhs, l_a, nb_sf_free, l_ipiv, l_b, nb_sf_free, l_info )
!print *, l_info
      l_sf(:) = l_sf(:) + l_b(:,1)

      call calc_std_dev_core  ( l_sf, tab_std_dev )
      call calc_grad_std_dev  ( l_sf, grad_std_dev )
      call calc_jacob_std_dev ( l_sf, jacob_std_dev )

      l_iter = l_iter + 1
!print '(I4,100ES10.2)', l_iter, l_sf(:), tab_std_dev(1), tab_std_dev(2), grad_std_dev(:)
      ! Convergence problem
      if ( l_iter .ge. nb_iter_max ) then
        write ( unit=0, fmt='()' ) 
        write ( unit=0, fmt='(" ERROR : in calc_std_err,")' ) 
        write ( unit=0, fmt='(" Convergence not reached after ",I0, &
                & " iterations")' ) nb_iter_max
        write ( unit=0, fmt='()' ) 
        stop 4
      end if
    end do

    ! Output printings
    write ( *, * )
    write ( *, '("  Minimal linear std. dev. = ",ES9.3)' ) tab_std_dev(1)
    do j1 = 1, nb_sf_free
      l_sf_sensit(j1) = sqrt(2.d0*eps_sensit*tab_std_dev(1)/jacob_std_dev(j1,j1))
      print '("   - free s.f. #",I0," = ",F5.3," +/- ",F5.3)', j1, l_sf(j1), &
                                                               l_sf_sensit(j1)
    end do
    write ( *, * )
  end subroutine find_std_dev_min
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine calc_grad_log_std_dev ( l_sf, l_grad )
    ! Calculates the gradient of log std dev with respect to free s.f.s
    double precision, dimension(:), intent(in) :: l_sf
    double precision, dimension(:), intent(out) :: l_grad
    integer :: j1, j2, j3

    ! Builds arrays with all s.f.s
    do j1 = 1, nb_sf_tot
      d_sf_all(j1) = l_sf(tab_sf_cstr(j1))
    end do

    ! Scans vector components
    l_grad(:) = 0.d0
    do j2 = 1, nb_exp_line
      Jup   = tab_Jup(j2)
      A_exp = tab_A_exp(j2)
      A_th = dot_product ( tab_slope_sf(:,j2), d_sf_all(:) )
      A_th = A_th**2 / dble(2*Jup+1)
      do j1 = 1, nb_sf_free
        do j3 = 1, nb_sf_tot
          if ( tab_sf_cstr(j3) .eq. j1 ) &
            l_grad(j1) = l_grad(j1) + tab_slope_sf(j3,j2)/sqrt(dble(2*Jup+1)) &
                       * log10(A_th/A_exp) / sqrt(A_th)
        end do
      end do
    end do
    l_grad(:) = 2.d0/log(10.d0)/tab_std_dev(2)/dble(nb_exp_line-nb_sf_free) * l_grad(:)
  end subroutine calc_grad_log_std_dev
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine calc_jacob_log_std_dev ( l_sf, l_jac )
    ! Calculates the jacobian matrix of log std dev with respect to free s.f.s
    double precision, dimension(:), intent(in) :: l_sf
    double precision, dimension(:,:), intent(out) :: l_jac
    double precision :: A_th_j, A_exp_j, A_th_k, A_exp_k
    real :: Jup_j, Jup_k
    integer :: j1, j2, j3, k1, k2, k3

    ! Builds arrays with all s.f.s
    do j1 = 1, nb_sf_tot
      d_sf_all(j1) = l_sf(tab_sf_cstr(j1))
    end do

    ! Part depending on 1 transition
    l_jac(:,:) = 0.d0
    do j2 = 1, nb_exp_line
      Jup   = tab_Jup(j2)
      A_exp = tab_A_exp(j2)
      A_th  = dot_product ( tab_slope_sf(:,j2), d_sf_all(:) )
      A_th  = A_th**2 / dble(2*Jup+1)
      do j1 = 1, nb_sf_free
        do j3 = 1, nb_sf_tot
          if ( tab_sf_cstr(j3) .eq. j1 ) then
            do k1 = j1, nb_sf_free
              do k3 = 1, nb_sf_tot
                if ( tab_sf_cstr(k3) .eq. k1 ) then
                  l_jac(j1,k1) = l_jac(j1,k1) + (2.d0/log(10.d0)-log10(A_th/A_exp))/A_th &
                   * tab_slope_sf(j3,j2)*tab_slope_sf(k3,j2)/dble(2*Jup+1) &
                   * 2.d0/log(10.d0)/tab_std_dev(2)/dble(nb_exp_line-nb_sf_free)
                end if
              end do
            end do
          end if
        end do
      end do
    end do
    ! Part depending on 2 transitions
    do j2 = 1, nb_exp_line
      Jup_j   = tab_Jup(j2)
      A_exp_j = tab_A_exp(j2)
      A_th_j  = dot_product ( tab_slope_sf(:,j2), d_sf_all(:) )
      A_th_j  = A_th_j**2 / dble(2*Jup_j+1)
      do k2 = 1, nb_exp_line
        Jup_k   = tab_Jup(k2)
        A_exp_k = tab_A_exp(k2)
        A_th_k  = dot_product ( tab_slope_sf(:,k2), d_sf_all(:) )
        A_th_k  = A_th_k**2 / dble(2*Jup_k+1)
        ! Scans rows and columns
        do j1 = 1, nb_sf_free
          do j3 = 1, nb_sf_tot
            if ( tab_sf_cstr(j3) .eq. j1 ) then
              do k1 = j1, nb_sf_free
                do k3 = 1, nb_sf_tot
                  if ( tab_sf_cstr(k3) .eq. k1 ) then
                    l_jac(j1,k1) = l_jac(j1,k1) &
                     - 4.d0 / (log(10.d0))**2 / tab_std_dev(2)**3 &
                     / dble(nb_exp_line-nb_sf_free)**2 &
                     * tab_slope_sf(j3,j2)/sqrt(dble(2*Jup_j+1)) &
                     * log10(A_th_j/A_exp_j) / sqrt(A_th_j) &
                     * tab_slope_sf(k3,k2)/sqrt(dble(2*Jup_k+1)) &
                     * log10(A_th_k/A_exp_k) / sqrt(A_th_k)
                  end if
                end do
              end do
            end if
          end do
        end do
      end do
    end do
    ! Now fills upper triangle
    do j1 = 1, nb_sf_free
      do k1 = 1, (j1-1)
        l_jac(j1,k1) = l_jac(k1,j1)
      end do
    end do
  end subroutine calc_jacob_log_std_dev
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine find_log_std_dev_min ( l_sf, l_sf_sensit )
    double precision, dimension(:), intent(inout) :: l_sf, l_sf_sensit
    integer, dimension(:), allocatable :: l_ipiv
    double precision, dimension(:,:), allocatable :: l_a, l_b
    integer :: l_nrhs = 1, l_info, l_iter, j1

    allocate ( l_ipiv(nb_sf_free), l_a(nb_sf_free,nb_sf_free), l_b(nb_sf_free,l_nrhs) )

    l_iter = 0
    call calc_std_dev_core  ( l_sf, tab_std_dev )
    call calc_grad_log_std_dev  ( l_sf, grad_std_dev )
    call calc_jacob_log_std_dev ( l_sf, jacob_std_dev )

    ! Test convergence to solution
    do while ( all ( (abs(grad_std_dev(:))) .gt. &
                  (/ (eps_grad, j1=1, nb_sf_free) /) ) )

      l_a(:,:) = jacob_std_dev(:,:)
      l_b(:,1) = -grad_std_dev(:)
      call DGESV ( nb_sf_free, l_nrhs, l_a, nb_sf_free, l_ipiv, l_b, nb_sf_free, l_info )
      l_sf(:) = l_sf(:) + l_b(:,1)

      call calc_std_dev_core  ( l_sf, tab_std_dev )
      call calc_grad_log_std_dev  ( l_sf, grad_std_dev )
      call calc_jacob_log_std_dev ( l_sf, jacob_std_dev )

      l_iter = l_iter + 1
!print '(I4,100ES10.2)', l_iter, l_sf(:), tab_std_dev(1), tab_std_dev(2), grad_std_dev(:)
      ! Convergence problem
      if ( l_iter .ge. nb_iter_max ) then
        write ( unit=0, fmt='()' ) 
        write ( unit=0, fmt='(" ERROR : in calc_std_err,")' ) 
        write ( unit=0, fmt='(" Convergence not reached after ",I0, &
                & " iterations")' ) nb_iter_max
        write ( unit=0, fmt='()' ) 
        stop 5
      end if
    end do

    ! Output printings
!    write ( *, * )
    write ( *, '("  Minimal logarithmic std. dev. = ",ES9.3)' ) tab_std_dev(2)
    do j1 = 1, nb_sf_free
      l_sf_sensit(j1) = sqrt(2.d0*eps_sensit*tab_std_dev(2)/jacob_std_dev(j1,j1))
      print '("   - free s.f. #",I0," = ",F5.3," +/- ",F5.3)', j1, l_sf(j1), &
                                                               l_sf_sensit(j1)
    end do
  end subroutine find_log_std_dev_min
  !!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!
end program calc_std_dev

