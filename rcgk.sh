#!/bin/bash
#
# Shell to run "rcgk", the Kramida version of RCG.
#
# Usage: rcgk.sh Atom
#
# It searches for the input file Atom.ing11, as well as
# FOR072, FOR073, FOR074, SENIOR. It generates
# Atom.OUT and Atom.OUTGINE .
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

# If no argument, type Atom in stdin
Atom=$1
if [ "$Atom" = "" ]; then
	echo -e "No file name specified\n\nEnter input file name (without extension):"
	read Atom
fi

# Test existence of input file for RCN
InFile=$Atom.ING11
if [[ ! -f $InFile ]]; then
  echo -e "\nERROR: file $InFile does not exist !\n"
  exit 1
fi

# Test existence of FOR07[2-4]
fold_cfp="/work/shared/icb/atom_phys/bin/Cowan_PC_2018/CODE/"
for file in FOR072 FOR073 FOR074 SENIOR; do
#  echo "$fold_cfp$file"
  if [[ ! -f $file ]]; then
    cp "$fold_cfp$file" .
  fi
done

cp $InFile ING11
rcgk
mv OUTG11 $Atom.OUT
mv OUTGINE $Atom.OUTGINE
rm ING11

