# ana_line_detail.awk
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

BEGIN{
  nb_lin_ign = 2
  delta_stdev_log = 0.1
}

( FNR>nb_lin_ign ) {
  ind = int( $9/delta_stdev_log + 0.5 )
  distr[ind] = distr[ind] + 1
}

END {
  s = 0.e0
  for ( ind in distr ) {
#print ind*delta_stdev_log, distr[ind]/NR
    if ( ind == "-0" ) {
      distr_m0 = distr[ind]
#print distr_m0
    }
    else {
      if ( ind == "0" ) {
#print distr[ind]
        distr[ind] = distr_m0 + distr[ind]
#print distr[ind]
      }
      s = s + distr[ind]
      printf ( "%5.2f %7.4f\n", ind*delta_stdev_log, distr[ind]/NR )
    }
  }
#print s/NR
}

