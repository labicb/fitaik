! mod_param.f90
! 
! Defines the Fortran module called "param",
! containing parameters common to different subprograms.
! 
! Copyright 2021,2022 Maxence Lepers
! 
! LICENSE:
! 
!    This file is part of FitAik.
! 
!    FitAik is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    any later version.
! 
!    FitAik is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
! 
!    You should have received a copy of the GNU General Public License
!    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
! 

module param
  implicit none
  integer, parameter :: tol_ener = 8 ! energy tolerence in 0.1*cm-1
  character(len=4) :: atom
  character(len=64) :: name_inp_file_ener_1, name_inp_file_ener_2, &
                       name_inp_file_spec_exp, name_inp_file_spec_th, &
                       name_file_index_line, pattern_file_spec_sf, &
                       name_out_stddev_file, name_file_fit_aik, &
                       name_out_sf_opt_file
    ! Names of data files
  integer :: rec_len_spec_exp, rec_len_spec_th = 133
  character(len=4) :: parity_1, parity_2
    ! First and second parity in theoretical lines
  character(len=96) :: form_data_ener, form_data_spec_exp, form_data_spec_th
    ! format of input data
  character(len=64) :: order_data_spec_exp, order_data_spec_th
    ! Order ib which data appears : for exp lines, a combination of
    ! "low" "up" "E" "A"; - for th ones, a combination of "1" "2" "E" "A"
  logical :: rand_Aik_exp ! true if Aik must be multiplied by a random factor
                          ! to cover their range of uncertainty, =F otherwise
  logical :: rand_Aik_exp_prctg ! true if this range of uncertainty is given
                                ! as percentages

  double precision :: unit_Aik_exp
    ! Units of s-1 in which exptal Einstein coefficients are given

  ! Variables concerning scaling factors
  integer :: sf_denom
    ! denominator of s.f.s by which the integer arguments must be divided
  integer, parameter :: nb_sf_tot_max = 150
    ! max tot number of scaling factors (to allocate "tab_sf_line" and
    ! "tab_sf_cstr" before namelist reading
  integer :: nb_sf_free, nb_sf_tot
    ! number of free and constrained scaling factors
  integer :: nb_sf_cow, sf_min_cow, sf_step_cow
    ! Nber, minimum and step of scaling factors for Cowan calculation
  integer, dimension(nb_sf_tot_max) :: tab_sf_min, tab_sf_max, tab_sf_step
    ! arrays of dimension nb_sf_tot_max, giving the min, max and step
    ! for each free scaling factor
  integer, dimension(nb_sf_tot_max) :: tab_sf_line, tab_sf_cstr
    ! arrays of dimension nb_sf_tot_max, giving the lines of ing11 file
    ! where to find s.f.s and the indices of constraints
    ! (1<=tab_sf_cstr(:)<=nb_sf_free)
  integer, parameter :: col_ing11_min = 39, col_ing11_max = 50
    ! min and max numero of column to modify
  character(len=*), parameter :: form_rij = '(F12.7)'
    ! F90 format of r_ij in ing11 files
  character(len=12) :: pattern_model_ing11
  character(len=6 ) :: pattern_out_ing11
    ! Pattern for ing11
  character(len=*), parameter :: pattern_out_line_detail = "line_detail"
    ! Pattern for file giving errors for each line

 !::::::::::::::
 contains
 !::::::::::::::
  subroutine read_namelist_param ( file_unit )
    ! Imports variables of namelist "/list_param/", from unit "file_unit"
    integer, intent(in) :: file_unit

    ! Reads parameters from namelist
    namelist /list_param/ atom, name_inp_file_ener_1, name_inp_file_ener_2, &
      name_file_index_line, pattern_file_spec_sf, name_out_stddev_file, &
      name_file_fit_aik, name_out_sf_opt_file, &
      pattern_model_ing11, pattern_out_ing11, &
      parity_1, parity_2, form_data_ener, name_inp_file_spec_exp, form_data_spec_exp, &
      order_data_spec_exp, rand_Aik_exp, rand_Aik_exp_prctg, name_inp_file_spec_th, &
      form_data_spec_th, order_data_spec_th, sf_denom, nb_sf_free, nb_sf_tot, &
      nb_sf_cow, sf_min_cow, sf_step_cow, tab_sf_min, tab_sf_max, tab_sf_step, &
      tab_sf_line, tab_sf_cstr, unit_Aik_exp
    read ( unit=file_unit, nml=list_param )
  end subroutine read_namelist_param
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function print_name_spec_th_file_core ( tab_sf )
    ! Prints the name of theor. spec. file, from scaling factors in input
    ! Equal n subsequent scaling factors give the pattern "xn" (times n)
    integer, dimension(:) :: tab_sf
    character(len=128) :: print_name_spec_th_file_core, s1, s2
    integer :: i1, i2, i3

    ! Inialization
    i2 = int(log10(real(sf_denom))+0.1) + 1 ! nber of digits for sfs.
    print_name_spec_th_file_core = trim(atom) // "_" // trim(pattern_file_spec_sf)
    ! 1st scaling factor, necessarily writen in file name
    i1 = 1
    s1 = trim(print_name_spec_th_file_core) // "_"
    write ( s2, '("(A,I0.",I0,")")' ) i2
    write ( unit=print_name_spec_th_file_core, fmt=s2 ) trim(s1), tab_sf(i1)
    i3 = 1

    ! At each step, checks if the series of equal subsequent s.f.s
    ! continues or not
    do i1 = 2, size(tab_sf,dim=1)
      ! -------- It continues -----------
      if ( tab_sf(i1) .eq. tab_sf(i1-1) ) then
        ! But if we reach the last s.f., prints
        i3 = i3 + 1
        if ( i1 .eq. size(tab_sf,dim=1) ) then
          s1 = trim(print_name_spec_th_file_core) // "x"
          s2 = '(A,I0)'
          write ( unit=print_name_spec_th_file_core, fmt=s2 ) trim(s1), i3
        end if
      ! -------- There is a stopping series ---------
      else if ( i3 .gt. 1 ) then
        ! So prints "times n" pattern ...
        s1 = trim(print_name_spec_th_file_core) // "x"
        s2 = '(A,I0)'
        write ( unit=print_name_spec_th_file_core, fmt=s2 ) trim(s1), i3
        i3 = 1
        ! ... and prints new s.f.
        s1 = trim(print_name_spec_th_file_core) // "_"
        write ( s2, '("(A,I0.",I0,")")' ) i2
        write ( unit=print_name_spec_th_file_core, fmt=s2 ) trim(s1), tab_sf(i1)
      ! -------- There is no series ---------
      else
        ! Just prints running s.f.
        s1 = trim(print_name_spec_th_file_core) // "_"
        write ( s2, '("(A,I0.",I0,")")' ) i2
        write ( unit=print_name_spec_th_file_core, fmt=s2 ) trim(s1), tab_sf(i1)
      end if
      ! -------------------------------------
    end do
    print_name_spec_th_file_core = trim(print_name_spec_th_file_core) // ".txt"
  end function print_name_spec_th_file_core
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module param

