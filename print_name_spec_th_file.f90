! print_name_spec_th_file.f90
! 
! Program that prints the name of the treoretical spectrum file,
! from the set of scaling factors in input.
! 
! Copyright 2021,2022 Maxence Lepers
! 
! LICENSE:
! 
!    This file is part of FitAik.
! 
!    FitAik is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    any later version.
! 
!    FitAik is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
! 
!    You should have received a copy of the GNU General Public License
!    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
! 

program print_name_spec_th_file
  use param
  implicit none
  integer, dimension(:), allocatable :: tab_sf
  integer :: i, nb_sf
  character(len=128) :: s

  call read_namelist_param ( 5 )

  nb_sf = command_argument_count()
  allocate ( tab_sf(nb_sf) )
  do i = 1, nb_sf
    call get_command_argument ( i, s )
    read ( s, * ) tab_sf(i)
  end do
  print '(A)', trim(print_name_spec_th_file_core(tab_sf)) 
  deallocate ( tab_sf )
end program print_name_spec_th_file

