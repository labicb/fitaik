! write_file_ing11.f90
! 
! Writes the input file ".ing11", for multiple calls of 
! 'Cowan rcg" with different scaling factors.
! 
! The values of the scaling factors are given in arguments,
! Other parameters from namelist given in std input.
! 
! Copyright 2021,2022 Maxence Lepers
! 
! LICENSE:
! 
!    This file is part of FitAik.
! 
!    FitAik is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    any later version.
! 
!    FitAik is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
! 
!    You should have received a copy of the GNU General Public License
!    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
! 

program write_file_ing11
  use param
  implicit none

  integer :: i1, i2, line_old
  integer, dimension(:), allocatable :: tab_sf
  real   , dimension(:), allocatable :: tab_rij_model
  character(len=128) :: s1, name_model_ing11_file, name_out_ing11_file
  real :: r1
  logical :: test_open_file


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !          Reads and prints parameters          !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! Read from namelist /last_param/ in std input (hence the 5)
  call read_namelist_param ( 5 )
  ! Then prints them in stdout
  write ( *, fmt='("")' )
  write ( *, fmt='(" Denominator of scaling factors : ",I0)' ) sf_denom
  write ( *, fmt='(" Total nber of s.f.s :",I3)' ) nb_sf_tot
!print *,trim(s1)
  s1 = '(" Lines to modify in ing11 :",   I4)'
  write ( s1(32:34), '(I3)' ) nb_sf_tot
  write ( unit=*, fmt=trim(s1) ) tab_sf_line(1:nb_sf_tot)
  write ( *, fmt='("")' )


  ! Raads command-line arguments and allocation
  i1 = command_argument_count()
  if ( nb_sf_tot .ne. i1 ) then
    write ( unit=0, fmt='()' )
    write ( unit=0, fmt='(" ERROR : in write_file_ing11")' )
    write ( unit=0, fmt=&
       '(" Invalid nber of arguments = ",I0, ", while ",I0," are expected !")' ) & 
       i1, nb_sf_tot
    write ( unit=0, fmt='()' )
    stop 1
  end if
  allocate ( tab_rij_model(nb_sf_tot), tab_sf(nb_sf_tot) )
  do i1 = 1, nb_sf_tot
    call get_command_argument ( i1, s1 )
    read ( s1, * ) tab_sf(i1)
    write ( *, fmt='(" Scaling factor #",I0," : ",F9.5)' ) i1, &
       real(tab_sf(i1))/real(sf_denom)
  end do
  write ( *, fmt='()' )


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !          Writes ing11 file from model         !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! Checks files existence and opens them
  name_model_ing11_file = trim(atom) // pattern_model_ing11
  inquire ( exist=test_open_file, file=trim(name_model_ing11_file) )
  if ( .not. test_open_file ) then
    write ( unit=0, fmt=* )
    write ( unit=0, fmt='(" File ",A," not found !")' ) trim(name_model_ing11_file)
    write ( unit=0, fmt=* )
    stop 1
  end if
  name_out_ing11_file   = trim(atom) // pattern_out_ing11
  open ( unit=1, action="read" , file=trim(name_model_ing11_file) )
  open ( unit=2, action="write", file=trim(name_out_ing11_file) )

  ! Scans input file until a line to modify is reached
  line_old = 0
  do i1 = 1, nb_sf_tot
    do i2 = 1, (tab_sf_line(i1)-line_old-1)
      read ( unit=1, fmt='(A128)' ) s1
      write ( unit=2, fmt='(A)' ) trim(s1)
    end do
    read ( unit=1, fmt="(A128)" ) s1
    read ( unit=s1(col_ing11_min:col_ing11_max), fmt=form_rij ) tab_rij_model(i1)
    r1 = real(tab_sf(i1)) / real(SF_denom) * tab_rij_model(i1)
    write ( unit=s1(col_ing11_min:col_ing11_max), fmt=form_rij ) r1
    write ( unit=2, fmt='(A)' ) trim(s1)
    line_old = tab_sf_line(i1)
  end do
  ! Scans as copies as is the rest of the input file
  do
    read ( unit=1, fmt='(A128)', end=1 ) s1
    write ( unit=2, fmt='(A)' ) trim(s1)
  end do
1 continue

  deallocate ( tab_sf, tab_rij_model )
  close ( unit=1 )
  close ( unit=2 )
end program write_file_ing11

