# makefile
# 
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

PROGS = $(PROG1) $(PROG2) $(PROG3) $(PROG4) $(PROG5)

PROG1 = calc_std_dev
PROG2 = index_th_lines
PROG3 = write_file_ing11
PROG4 = print_name_spec_th_file
PROG5 = fit_aik

SRC1 = mod_param.f90 calc_std_dev.f90
SRC2 = mod_param.f90 index_th_lines.f90
SRC3 = mod_param.f90 write_file_ing11.f90
SRC4 = mod_param.f90 print_name_spec_th_file.f90
SRC5 = mod_param.f90 fit_aik.f90

LIB1 = -llapack
LIB2 =
LIB3 =
LIB4 =
LIB5 =

F77C = gfortran
F77FLAGS = -O2
F90C = gfortran
F90FLAGS = -O2
CC = gcc
CFLAGS = -O2
LDFLAGS = -O2 -o


all: $(PROGS)

$(PROG1): $(SRC1)
	$(F90C) $(LDFLAGS) $@ $(SRC1) $(LIB1)

$(PROG2): $(SRC2)
	$(F90C) $(LDFLAGS) $@ $(SRC2) $(LIB2)

$(PROG3): $(SRC3)
	$(F90C) $(LDFLAGS) $@ $(SRC3) $(LIB3)

$(PROG4): $(SRC4)
	$(F90C) $(LDFLAGS) $@ $(SRC4) $(LIB4)

$(PROG5): $(SRC5)
	$(F90C) $(LDFLAGS) $@ $(SRC5) $(LIB5)


clean:
	rm *.dat $(PROGS) *.log *.mod
	rm tmp.in tmp_sf.in
	rm cowan/*.aa       cowan/*.eo   cowan/*.lab     cowan/*.lslk
	rm cowan/*.abseig   cowan/*.ls   cowan/*.out     cowan/*.wo
	rm cowan/*.decays   cowan/*.lsjk cowan/*.rad     cowan/*.unsorted
	rm cowan/*.eav      cowan/*.jj   cowan/*.lsjlks  cowan/*.sorted
	rm cowan/*.eig      cowan/*.jjjk cowan/*.lsjlsj  cowan/*.spec
	rm cowan/*.channels cowan/outg*  cowan/tape*     cowan/ing11
	rm cowan/*.OUT*     cowan/FOR07* cowan/TAPE*     cowan/SENIOR
	rm -r cowan/*/

cleanup:
	rm *SFs_*txt *Spec_th_ref.txt *fit_aik.txt *index_th_lines.txt

