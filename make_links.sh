#!/bin/bash
#
# Make symbolic links for the sources files in the 
# present directory, from the particular directory
# of a given atom/ion.
#
# Aargument : the atom/ion directory
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

DIST_DIR=$1
PROG_DIR=$PWD
FILE_LIST="makefile ana_line_detail.awk filter_spec.awk mod_param.f90 \
  calc_aver_sf.awk fit_aik.f90 fitaik.sh calc_std_dev.f90 index_th_lines.f90 \
  print_name_spec_th_file.f90 search_min.awk write_file_ing11.f90 \
  write_outgk_spec.awk rcgk.sh"

echo "Program directory : $PROG_DIR"
echo "Distant directory : $1"
echo
echo "File list $FILE_LIST"

cd $DIST_DIR
for i in $FILE_LIST; do
  ln -sf $PROG_DIR/$i $i
done
cd $PROG_DIR

