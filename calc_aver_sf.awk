# calc_aver_sf.awk
#
# Calculates the mean and rms scaling factors, after several
# shots with random experimental Aik (reflecting their uncertainties).
#
# Copyright 2021,2022 Maxence Lepers
#
# LICENSE:
#
#    This file is part of FitAik.
#
#    FitAik is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    FitAik is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
#

BEGIN {
  nb_lin_ign = 1
  nb_lin = 0
  sf_aver = 0.
  sf_rms  = 0.
  if ( i_col == "" ) {
    i_col = 1
  }
  if ( i_sf == "" ) {
    i_sf = 1
  }
}

# Calculates average sf
((ARGIND == 1) && (FNR > nb_lin_ign)) {
  nb_lin++
  sf_aver = sf_aver + $i_col
}

# Calculates rms
((ARGIND == 2) && (FNR > nb_lin_ign)) {
  if ( FNR == (nb_lin_ign+1) ) {
    sf_aver = sf_aver / nb_lin
  }
  sf_rms  = sf_rms + ($i_col-sf_aver)**2
}

END {
  sf_rms  = sqrt ( sf_rms/nb_lin )
  printf ( "  Aver. s.f. #%d = %5.3f +/- %5.3f\n", i_sf, sf_aver, sf_rms )
  printf ( "%d ", (sf_aver*sf_den+0.5) ) >> file_tmp
}

