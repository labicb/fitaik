! fit_aik.f90
! 
! This code calculates the coefficients between
! scaling factors and Einstein coefficients, by fitting
! the Aik's computed with several sets of SFs.
! 
! Copyright 2021,2022 Maxence Lepers
! 
! LICENSE:
! 
!    This file is part of FitAik.
! 
!    FitAik is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    any later version.
! 
!    FitAik is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
! 
!    You should have received a copy of the GNU General Public License
!    along with FitAik.  If not, see <https://www.gnu.org/licenses/>.
! 

program fit_aik
  use param
  implicit none
  double precision, parameter :: esp_aik = 0.9d-3
  integer, dimension(:  ), allocatable :: tab_unit_file_spec, tab_index_th_line
  integer, dimension(:,:), allocatable :: tab_all_sf
  double precision, dimension(:), allocatable :: tab_gA_th, tab_slope_sf
  double precision :: d1, d_sf_step_cow
  character(len=128) :: s1
  integer :: nb_exp_line, i_sf, i_cow, i_lin, i1
  real :: r1

  ! Read from namelist /last_param/ in std input (hence the 5)
  call read_namelist_param ( 5 )
  d_sf_step_cow = dble(sf_step_cow)/dble(sf_denom)
!print * !, d_sf_step_cow

  ! Imports indices of th lines
  open ( unit=1, action="read", file=trim(name_file_index_line) )
  ! First scan to determine nber of exptal lines in fit
  i_lin = 0
  do
    read ( unit=1, fmt=*, end=1 )
    i_lin = i_lin + 1
  end do
1 continue
  nb_exp_line = i_lin
!print *, nb_exp_line
  allocate ( tab_index_th_line(nb_exp_line) )
  ! Second scan to import indices
  rewind ( unit=1 )
  do i_lin = 1, nb_exp_line
    read ( unit=1, fmt=* ) i1, tab_index_th_line(i_lin)
  end do
  close ( unit=1 )

  ! Opens spectrum files
  allocate ( tab_all_sf(0:nb_sf_cow*nb_sf_tot,nb_sf_tot) )
  allocate ( tab_unit_file_spec(0:nb_sf_cow*nb_sf_tot) )
  tab_unit_file_spec(0) = 10
  tab_all_sf(:,:) = sf_min_cow
!print *, tab_unit_file_spec(0), trim ( print_name_spec_th_file_core(tab_all_sf(0,:)) )
  open ( unit=tab_unit_file_spec(0), action="read", &
         file=trim(print_name_spec_th_file_core(tab_all_sf(0,:))), &
         form="formatted", recl=rec_len_spec_th, access="direct" )
  do i_sf = 1, nb_sf_tot
    do i_cow = 1, nb_sf_cow
      i1 = (i_sf-1)*nb_sf_cow + i_cow
      tab_unit_file_spec(i1) = 10 + i1
      tab_all_sf(i1,i_sf) = sf_min_cow + i_cow*sf_step_cow
      open ( unit=tab_unit_file_spec(i1), action="read", &
             file=trim(print_name_spec_th_file_core(tab_all_sf(i1,:))), &
             form="formatted", recl=rec_len_spec_th, access="direct" )
!print *, tab_unit_file_spec(i1), trim(print_name_spec_th_file_core(tab_all_sf(i1,:)))
    end do
  end do

  ! Processes theoretical lines with exptal counterpart
  open ( unit=2, action="write", form="unformatted", file=trim(name_file_fit_aik) )
  allocate ( tab_gA_th(0:nb_sf_cow), tab_slope_sf(nb_sf_tot) )
  do i_lin = 1, nb_exp_line
    tab_gA_th(:) = 0.d0
    ! First gA, for sf_min_cow
    !  NB: in theor spectrum we only keep gA
    read ( unit=tab_unit_file_spec(0), fmt=trim(form_data_spec_th), &
           rec=tab_index_th_line(i_lin) ) s1, r1, s1, r1, s1, r1, s1, r1, s1, r1, &
                                          s1, tab_gA_th(0)
!print '("- Trans. #",I0, " ; sqrt(A) = ",5ES11.3)', i_lin, sqrt(tab_gA_th(0))
    ! 
    ! Now scans scaling factors
    do i_sf = 1, nb_sf_tot
      ! Calculates slope for s.f. #i_sf
      i_cow = 1
      i1 = (i_sf-1)*nb_sf_cow + i_cow
      read ( unit=tab_unit_file_spec(i1), fmt=trim(form_data_spec_th), &
             rec=tab_index_th_line(i_lin) ) s1, r1, s1, r1, s1, r1, s1, r1, s1, r1, &
                                            s1, tab_gA_th(i_cow)
      tab_slope_sf(i_sf) = ( sqrt(tab_gA_th(1)) - sqrt(tab_gA_th(0)) ) &
                         / dble(sf_step_cow) * dble(sf_denom)
      ! 
      ! Checks slope with 2nd s.f. calculation
      i_cow = 2
      i1 = (i_sf-1)*nb_sf_cow + i_cow
      read ( unit=tab_unit_file_spec(i1), fmt=trim(form_data_spec_th), &
             rec=tab_index_th_line(i_lin) ) s1, r1, s1, r1, s1, r1, s1, r1, s1, r1, &
                                            s1, tab_gA_th(i_cow)
      d1 = ( sqrt(tab_gA_th(2)) - sqrt(tab_gA_th(1)) ) &
         / dble(sf_step_cow) * dble(sf_denom)
!print '("  * s.f. #",I0)', i_sf
!print '(5ES11.3)', sqrt(tab_gA_th(:)), tab_slope_sf(i_sf), d1
      ! Is there a pb?
      if ( (abs(d1-tab_slope_sf(i_sf))*d_sf_step_cow/sqrt(maxval(tab_gA_th(0:1))))&
            .ge. esp_aik ) then
!print '(I3)', i_lin
        ! If yes, tries slope with -sqrt(tab_gA_th(2)), since ||d||
        ! changes sign between i_sf=1 and 2
        d1 = ( -sqrt(tab_gA_th(2)) - sqrt(tab_gA_th(1)) ) &
           / dble(sf_step_cow) * dble(sf_denom)
        ! Is there still a pb?
        if ( (abs(d1-tab_slope_sf(i_sf))*d_sf_step_cow/sqrt(maxval(tab_gA_th(0:1))))&
              .ge. esp_aik ) then
          ! If yes, tries slope with -sqrt(tab_gA_th(1,2)), since ||d||
          ! changes sign between i_sf=0 and 1
        tab_slope_sf(i_sf) = ( -sqrt(tab_gA_th(1)) - sqrt(tab_gA_th(0)) ) &
                           / dble(sf_step_cow) * dble(sf_denom)
        d1 = ( -sqrt(tab_gA_th(2)) + sqrt(tab_gA_th(1)) ) &
           / dble(sf_step_cow) * dble(sf_denom)
          ! Is there still a pb?
          if ( (abs(d1-tab_slope_sf(i_sf))*d_sf_step_cow/sqrt(maxval(tab_gA_th(0:1))))&
                .ge. esp_aik ) then
            ! If yes, prints error
!print *, d1, tab_slope_sf(i_sf)
            write ( unit=0, fmt='()' )
            write ( unit=0, fmt='(" ERROR : in fit_aik, problem of scaling factor slope")' )
            write ( unit=0, fmt=&
                '("  exp. line #",I0, " ; s.f. #",I0," ; sqrt(gA) =",10ES11.3)' ) & 
                i_lin, i_sf, sqrt(tab_gA_th(:))
            write ( unit=0, fmt='()' )
            stop 1
          end if
        end if
      end if
    end do
!print *
    ! Prints slopes
    s1 = '(  ES14.6)'
    write ( unit=s1(2:3), fmt='(I2)' ) nb_sf_tot
!print *, "aa"
!    write ( unit=2, fmt=* ) tab_slope_sf(:)
    write ( unit=2 ) tab_slope_sf(:)
  end do


  close ( unit=2 )
  close ( unit=tab_unit_file_spec(0) )
  do i_sf = 1, nb_sf_tot
    do i_cow = 1, nb_sf_cow
      close ( unit=tab_unit_file_spec((i_sf-1)*nb_sf_cow+i_cow) )
    end do
  end do
  deallocate ( tab_all_sf, tab_unit_file_spec, tab_index_th_line, tab_gA_th )
  deallocate ( tab_slope_sf )
end program fit_aik


